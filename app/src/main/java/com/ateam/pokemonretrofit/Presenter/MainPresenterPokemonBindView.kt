package com.ateam.pokemonretrofit.Presenter

import com.ateam.pokemonretrofit.Model.ResponsePokemonData

interface MainPresenterPokemonBindView {
    fun onSuccess(data: ResponsePokemonData?)
    fun onError(msg : String)
}