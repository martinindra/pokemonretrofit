package com.ateam.pokemonretrofit.View.Main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.ateam.pokemonretrofit.Model.ResponsePokemonData
import com.ateam.pokemonretrofit.Presenter.MainPresenterPokemon
import com.ateam.pokemonretrofit.Presenter.MainPresenterPokemonBindView
import com.ateam.pokemonretrofit.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainPresenterPokemonBindView {

    private lateinit var mainPokemonAdapter: MainPokemonAdapter
    private lateinit var mainPresenterPokemon: MainPresenterPokemon

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupComponent()
    }

    private fun setupComponent() {
        mainPresenterPokemon = MainPresenterPokemon(this@MainActivity)
        mainPresenterPokemon.getDataPokemon()
    }

    override fun onSuccess(data: ResponsePokemonData?) {
        Log.d("dataResult", data?.results.toString())
        mainPokemonAdapter = MainPokemonAdapter(data?.results,this@MainActivity)
        rv_main.apply {
            rv_main.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rv_main.setHasFixedSize(true)
            rv_main.adapter = mainPokemonAdapter
        }
    }

    override fun onError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}