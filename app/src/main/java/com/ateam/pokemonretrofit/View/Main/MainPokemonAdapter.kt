package com.ateam.pokemonretrofit.View.Main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ateam.pokemonretrofit.Model.ResultsItem
import com.ateam.pokemonretrofit.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_list_pokemon.view.*

class MainPokemonAdapter(var listDataPokemon: List<ResultsItem?>?, var context: Context) : RecyclerView.Adapter<MainPokemonAdapter.ViewHolder>() {
    private lateinit var imagePokemon : ArrayList<String?>
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imagePokemon = itemView.image_pokemon
        val namaPokemon = itemView.tv_nama_pokemon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_pokemon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var image = listDataPokemon?.get(position)
        Glide.with(context).load(getImageUrl(image?.url!!)).into(holder.imagePokemon)
        holder.namaPokemon.text = listDataPokemon?.get(position)?.name
    }

    override fun getItemCount(): Int = listDataPokemon?.size!!

    fun getImageUrl(url:String): String {
        val index = url.split("/".toRegex()).dropLast(1).last()
        return "https://pokeres.bastionbot.org/images/pokemon/$index.png"
    }


}